from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve
from .views import index

class unitTest(TestCase):
	def test_apakah_ada_blog_page(self):
		c = Client()
		response = c.get("/")
		self.assertEqual(response.status_code, 200)

	def test_apakah_pake_template_bloghtml(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'blog.html')

	def test_apakah_pake_fungsi_index(self):
		found = resolve("/")
		self.assertEqual(found.func, index)

	def test_apakah_ada_tulisan_judul(self):
		c = Client()
		response = c.get("/")
		content = response.content.decode('utf8')
		self.assertIn("My First Load Balancer", content)